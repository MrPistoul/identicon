defmodule Identicon do

	def main(input)do
	input
	|> hash_input
	|> pick_color
	|> build_grid
	|> filter_odd_square
	|> build_pixel_map
	|> draw_image
	|> save_image(input)
	end

	def save_image(image,input ) do
	  File.write("gravatar/#{input}.png", image)
	end

	def draw_image(%Identicon.Image{color: color , pixel_map: pixel_map}) do  # je ne met pas  = image car je ne vais pas rendre la structure image |> dans main
		image = :egd.create(250,250)                                            # egd library pour la creation d'image
		fill = :egd.color(color)
			Enum.each pixel_map, fn ({start, stop}) ->
				:egd.filledRectangle(image, start, stop, fill)
			end
		:egd.render(image)

	end

	def build_pixel_map(%Identicon.Image{grid: grid} = image) do
		pixel_map = Enum.map grid, fn({_code, index}) ->      # pour chaque square de mon grid
		horizontal = rem(index, 5) * 50           # calcul par rapport a l'index de son emplacement 50 pour un square de 50 pixel et 5 car grid 5 x5 square
		vertical = div(index, 5) * 50

		top_left = {horizontal, vertical}
		bottom_right = {horizontal + 50 , vertical + 50}

		{top_left, bottom_right}

		end

			%Identicon.Image{image | pixel_map: pixel_map}
	end

	def filter_odd_square(%Identicon.Image{grid: grid} = image) do
	grid = Enum.filter(grid, fn({code, _}) -> ## code est le binaire et _ est l'index (j'en ai pas besoin)
		rem(code,2) == 0                 ## rem => reste de la division si == 0 => nbre pair , je retourne une nouvelle grid avec juste les nombre pairs
	end)
	%Identicon.Image{image | grid: grid}
	end

def build_grid(%Identicon.Image{hex: hex} = image) do  # je n'ai besoin que de hex (16 nombres pour la grille)
	grid =
				hex
				|> Enum.chunk(3)              # je separe la list en une list de list de 3 valeurs (partie gauche de la grille 3X5)
				|> Enum.map(&mirror_row/1)    # j'applique la symetrie a chaque list contenu dans la list
				|> List.flatten								# mirror_row peut etre pris pour un argument donc le & indique une reference a une function
				|> Enum.with_index						# et /1 indique que c'est la function mirror_row qui prend un argument car il peu y avoir plusieurs function mirrow_row
																# flatten => 1 seul list
																# transform la list en list de Tuple [{156,0},{45,1}.... avec en second parameter
																# j'ai ajouter grid dans ma structure est je rajoute grid = en debut de fonction

				%Identicon.Image{image | grid: grid}    # je return ma structure au complet et j'afffecte grid a grid
end

def mirror_row(row)do
  # [1,2,3]
  [first, second | _tails] = row
  # [1,2,3,2,1]
  row ++ [second, first]
end
#def pick_color(image)do
#  %Identicon.Image{hex: hex_list} = image  ## encore la magie du pattern matching, je recré une nouvelle structure (et oui pas de mutation)
#  [r,g,b | _tail] = hex_list               ##  | _tail => il y a 15 nombres et je ne vais pas ecrire [r,g,b,1,2,3,....  je ne veux que les 3 premiers
#  [r,g,b]
#end

#def pick_color(image) do
#  %Identicon.Image{hex: [r,g,b | _tail]} = image    # plus court
#  %Identicon.Image{image | color: {r,g,b}}  ## j'ai modif ma struct dans image.ex et donc je return a image.ex image == hex et color
#end

def pick_color( %Identicon.Image{hex: [r,g,b | _tail]} = image ) do
  %Identicon.Image{image | color: {r,g,b}}                                 # encore plus court , ça deviend chaud le pattern matching !
end

## crypto return une serie de 16 nombre , avec binary to list , je reformat en list [], les 3 premiers feront la couleur RGB
## ma grille fait 5 x 5 et l'identicon sera symetrique donc cela fait deux grilles
## de 5 x 3 => mes 15 premiers nombres. La colone centrale sera superposé, les nombres pairs seront colorés
	def hash_input(input) do
		hex = :crypto.hash(:md5, input)
		|> :binary.bin_to_list

		%Identicon.Image{hex: hex}  ## envoie hex dans la structure hex (fichier image.ex), une structure attend une uniquement des data avec les clé defini dans la structure ex: hex
																## la difference entre une structure et une map (meme syntaxe). une structure, on declare des propriété par default , on ne peu aps en rajouter


	end



end
